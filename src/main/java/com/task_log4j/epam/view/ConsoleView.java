package com.task_log4j.epam.view;

import com.task_log4j.epam.model.Student;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView implements View {

    private final static Logger logger = LogManager.getLogger(ConsoleView.class);

    public void showStudent(Student student) {
        logger.info("Student name :" + student.getName() + "; " +
                "Student age:" + student.getAge());
    }
}
