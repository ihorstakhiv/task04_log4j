package com.task_log4j.epam.view;

import com.task_log4j.epam.model.Student;

public interface View {

    void showStudent(Student student);
}
