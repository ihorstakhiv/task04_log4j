package com.task_log4j.epam.model;

public interface ModelLayer {

    Student getStudent();
}
