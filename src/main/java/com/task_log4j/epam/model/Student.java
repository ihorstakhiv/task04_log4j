package com.task_log4j.epam.model;

public class Student {

    String name;
    int age;

    public Student() {
        this.name = "Ihor";
        this.age = 23;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
