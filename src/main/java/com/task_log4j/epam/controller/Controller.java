package com.task_log4j.epam.controller;

import com.task_log4j.epam.model.DBLayer;
import com.task_log4j.epam.model.ModelLayer;
import com.task_log4j.epam.model.Student;
import com.task_log4j.epam.view.ConsoleView;
import com.task_log4j.epam.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {

    private final static Logger logger = LogManager.getLogger(Controller.class);

    ModelLayer modelLayer = new DBLayer();
    View view = new ConsoleView();

    public void execute() {
        Student student = modelLayer.getStudent();
        view.showStudent(student);
        logger.error("Error");
        logger.fatal("Fatal");
        logger.warn("Warn");
    }
}
