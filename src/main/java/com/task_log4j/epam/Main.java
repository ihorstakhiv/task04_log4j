package com.task_log4j.epam;

import com.task_log4j.epam.controller.Controller;

public class Main {

    public static void main(String[] args) {
        new Controller().execute();
    }
}
