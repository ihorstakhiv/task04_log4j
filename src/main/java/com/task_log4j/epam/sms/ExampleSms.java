package com.task_log4j.epam.sms;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSms {

    public static final String ACCOUNT_SID = "AC84bf097a37fdf14dd3cc4614cf704c7a";
    public static final String AUTH_TOKEN = "ee8ba247c8a651cfc9cdc79d1b7b8070";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380972118111"), /*my phone number*/
                        new PhoneNumber("+1 561 935 1567"), str).create(); /*attached to me number*/
    }
}
